from datetime import datetime
#import googlemaps

#Read in the CSV and then close it
crime_location_file = open("crimelocationfile.csv", "r")
crime = crime_location_file.read()
crime_location_file.close()

#Create a list from the comma seperated value string
crime_list = crime.split("\n")
crime_list.sort()

#Split Latitude and Longitude into their own lists
latitudes = []
longitudes = []
for item in crime_list:
    latitudes.append(item.split(",")[0])
    longitudes.append(item.split(",")[1])

print "Starting location: "
location_start = raw_input()
print "Ending location: "
location_finish = raw_input()

##This is where you would get the walking path
##gmaps = googlemaps.Client(key='Add Your Key here')
##now = datetime.now()
##directions_results = gmaps.directions(location_start, location_end, mode="walking", departure_time=now)

#Example numbers
start_latitude = 41.64458972
end_latitude = 41.7
start_longitude = -87.524616
end_longitude = -87.524627
path_segments = 1

#Iteration for each step in the path
loop = 0
while  loop < path_segments:
    loop = loop + 1

    #Upper and lower are the bounds of the list that have values within the range
    lower = -1
    upper = -1

    #Find the lower bound for Latitude
    i = 0
    x = len(latitudes)
    while i < x:
        if start_latitude <= float(latitudes[i]):
            lower = i
            break
        i = i+1

    #Find the upper bound for Latitude
    i = lower
    while i < x: 
        if end_latitude < float(latitudes[i]):
            upper = i - 1
            break
        i = i + 1

    #We only want the section of the longitudes list that matches the section of the latitudes list we identified to be within the bounds of the segment of the path
    longitudes_in_range = []
    i = lower
    while i <= upper:
        longitudes_in_range.append(longitudes[i])
        i = i+1

    #Sort the list of Longitudes
    longitudes_in_range.sort()

    #Reset the upper and lower bounds
    lower = -1
    upper = -1

    #Find the lower bound for Longitude
    i = 0
    while i < len(longitudes_in_range):
        if start_longitude >= float(longitudes_in_range[i]):
            lower = i
            break
        i = i+1

    #Find the upper bound for Longitude
    i = lower
    while i < len(longitudes_in_range): 
        if end_longitude >= float(longitudes_in_range[i]):
            upper = i - 1
            break
        i = i+1

    #Change to the next Latitude and Longitude
    start_latitude = 41.64458972
    end_latitude = 41.7
    start_long = -87.524616
    end_long = -87.524627

amount = upper - lower + 1
print "Amount of crimes on this path is %s" %amount
